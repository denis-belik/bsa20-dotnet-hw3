﻿using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using Newtonsoft.Json;


namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        IParkingService _parkingService;
        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("balance")]
        public IActionResult Balance() => new JsonResult(_parkingService.GetBalance());

        [HttpGet("capacity")]
        public IActionResult Сapacity() => new JsonResult(_parkingService.GetCapacity());
  
        [HttpGet("freePlaces")]
        public IActionResult FreePlaces() => new JsonResult(_parkingService.GetFreePlaces());    
    }
}
