﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using System.IO;
using CoolParking.WebAPI.Models;


namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        IParkingService _parkingService;
        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public IActionResult GetVehicles() => new JsonResult(_parkingService.GetVehicles());

        [HttpGet("{id}")]
        public IActionResult GetVehicle(string id)
        {
            if (!Vehicle.IsIdValid(id))
                return BadRequest($"Incorrect vehicle id format.");

            Vehicle vehicle = _parkingService.GetVehicle(id);

            if (vehicle == null)
                return NotFound($"Vehicle {id} is not in the parking lot.");

            //string json = JsonConvert.SerializeObject(vehicle);
            return new JsonResult(vehicle);
        }


        [HttpPost]
        public IActionResult AddVehicle([FromBody] VehicleModel vehicleModel)
        {
            try
            {
                Vehicle vehicle = new Vehicle(vehicleModel.Id, vehicleModel.VehicleType, vehicleModel.Balance);
                _parkingService.AddVehicle(vehicle);

                return Created($"api/vehicles/{vehicle.Id}", new JsonResult(vehicle).Value);
            }
            catch (Exception argEx)
            {
                return BadRequest(argEx.Message);
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            try
            {
                Vehicle vehicle = _parkingService.GetVehicle(id);
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException argEx)
            {
                if (argEx.ParamName == "id")
                    return BadRequest(argEx.Message);
                else
                    return NotFound(argEx.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }

  
}
