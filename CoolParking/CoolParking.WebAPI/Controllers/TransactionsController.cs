﻿using System;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        IParkingService _parkingService;
        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public IActionResult TransactionsLast() => new JsonResult(_parkingService.GetLastParkingTransactions());

        [HttpGet("all")]
        public IActionResult TransactionsAll()
        {
            try
            {
                return new JsonResult(_parkingService.ReadFromLog());
            }
            catch(InvalidOperationException ioEx)
            {
                return NotFound(ioEx.Message);
            }
        }

        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicle([FromBody] TopUpModel topUpModel)
        {
            try
            {
                _parkingService.TopUpVehicle(topUpModel.Id, topUpModel.Sum);

                Vehicle vehicle = _parkingService.GetVehicle(topUpModel.Id);

                return new JsonResult(vehicle);
            }
            catch (ArgumentException argEx)
            {
                if (argEx.ParamName == "topupsum" | argEx.ParamName == "id")
                    return BadRequest(argEx.Message);
                else
                    return NotFound(argEx.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }  
}
