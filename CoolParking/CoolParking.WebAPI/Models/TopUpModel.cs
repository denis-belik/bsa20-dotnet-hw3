﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class TopUpModel
    {
        public string Id { get; set; }
        public decimal Sum { get; set; }
    }
}
