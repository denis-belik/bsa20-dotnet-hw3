﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Timers;
using System.Reflection;
using System.IO;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        readonly Parking parking = Parking.GetInstance();        
        
        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;

        private List<TransactionInfo> transactions = new List<TransactionInfo>();


        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;

            _withdrawTimer.Elapsed += MakeTransaction;
            _logTimer.Elapsed += LogLastTransactions;

            _withdrawTimer.Start();
            _logTimer.Start();
        }


        public decimal GetBalance() => parking.Balance;
        public int GetCapacity() => Settings.ParkingCapacity;
        public int GetFreePlaces() => Settings.ParkingCapacity - parking.Vehicles.Count;
        public ReadOnlyCollection<Vehicle> GetVehicles() => new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0)
                throw new InvalidOperationException($"The parking lot is full.");

            if (GetVehicle(vehicle.Id) == null)
                parking.Vehicles.Add(vehicle);
            else
                throw new ArgumentException($"Vehicle {vehicle?.Id} is already in the parking lot.");
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = GetVehicle(vehicleId);
            if (vehicle == null)
                throw new ArgumentException($"Vehicle {vehicleId} is not in the parking lot.", "vehicle");

            if (vehicle.Balance >= 0)
                parking.Vehicles.Remove(vehicle);
            else
                throw new InvalidOperationException($"Vehicle {vehicle.Id} has negative balance {vehicle.Balance}");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0)
                throw new ArgumentException($"Top up sum {sum} must be positive.", "topupsum");

            Vehicle vehicle = GetVehicle(vehicleId);

            if (vehicle != null)
                vehicle.Balance += sum;
            else
                throw new ArgumentException($"Vehicle {vehicleId} is not in the parking lot.", "vehicle");
        }

        public TransactionInfo[] GetLastParkingTransactions() =>  transactions.ToArray();

        public string ReadFromLog() => _logService.Read();

        public void Dispose()
        {
            parking.Vehicles = new List<Vehicle>();
            parking.Balance = 0;
        }

                    
        public void MakeTransaction(object sender, ElapsedEventArgs args)
        {
            foreach (Vehicle vehicle in parking.Vehicles)
            {
                decimal sum = CalculateVehicleTariff(vehicle);
                vehicle.Balance -= sum;
                parking.Balance += sum;

                TransactionInfo transaction = new TransactionInfo();
                transaction.VehicleId = vehicle.Id;
                transaction.Sum += sum;
                transaction.DateTime = DateTime.Now;
                transactions.Add(transaction);
            }
        }
        public void LogLastTransactions(object sender, ElapsedEventArgs args)
        {
            string transactionsInfo = GetLastTransactionsInfo();
            ResetLastTransactions();
            _logService.Write(transactionsInfo);
        }

             
        public Vehicle GetVehicle(string vehicleId)
        {
            if (Vehicle.IsIdValid(vehicleId))
                return parking.Vehicles.Find(vehicle => vehicle.Id == vehicleId);
            else
                throw new ArgumentException("Incorrect Id format.", "id");
        }

        public decimal CalculateVehicleTariff(Vehicle vehicle)
        {
            decimal tariff = Settings.Tariff[vehicle.VehicleType.ToString()];

            return vehicle.Balance >= tariff ? tariff :
                   vehicle.Balance <= 0 ? tariff * Settings.FineRatio :
                   vehicle.Balance - (vehicle.Balance - tariff) * Settings.FineRatio;
        }
        public string GetLastTransactionsInfo()
        {
            string lastTransactions = "Last transactions:\n";

            foreach (var transaction in transactions)
            {
                lastTransactions += string.Join("   ", transaction.DateTime, $"{transaction.VehicleId} {transaction.Sum}\n");
            }

            return lastTransactions;
        }
        public void ResetLastTransactions() => transactions = new List<TransactionInfo>();
        public decimal GetLastIncome()
        {
            decimal income = 0;

            foreach (var transaction in transactions)
                income += transaction.Sum;

            return income;
        }
    }
}


