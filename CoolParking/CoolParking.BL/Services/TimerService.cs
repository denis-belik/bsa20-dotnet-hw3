﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        Timer timer;

        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;
        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);
        }

        public TimerService(double interval)
        {
            Interval = interval;
        }

        public void Dispose()
        {
            timer.Dispose();
        }

        public void Start()
        {
            timer = new Timer(Interval);
            timer.Elapsed += Elapsed;
            timer.Start();
        }
        public void Stop()
        {
            timer.Stop();
            this.Dispose();
        }
    }
}
