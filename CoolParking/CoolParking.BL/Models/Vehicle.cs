﻿using System.Text.RegularExpressions;
using System;
using System.Linq;

// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = Regex.IsMatch(id, "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$") ? id
                 : throw new ArgumentException("Vehicle Id doesn't match the required pattern \"XX-YYYY-XX\".");

            int maxEnum = (int)Enum.GetValues(typeof(VehicleType)).Cast<VehicleType>().Last();
            VehicleType = (int)vehicleType <= maxEnum && (int)vehicleType >= 0 ? vehicleType
                          : throw new ArgumentException("Incorrect vehicle type");

            Balance = (balance >= 0) ? balance
                      : throw new ArgumentException("Vehicle Balance can not be negative.");
        }

        public static bool IsIdValid(string id) => Regex.IsMatch(id, "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
  
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random randomGenerator = new Random();

            char[] plateSymbols = new char[8];

            for (int i = 0; i < plateSymbols.Length / 2; i++)
            {
                plateSymbols[i] = (char)randomGenerator.Next(65, 91); // 4 first symbols are letters
                plateSymbols[i + 4] = (char)randomGenerator.Next(48, 58); // 4 last symbols are numbers
            }

            string[] plateNumberParts = new string[3]
            {
            string.Join("", plateSymbols[0..2]), // XX
            string.Join("", plateSymbols[4..8]), // YYYY
            string.Join("", plateSymbols[2..4])  // XX
            };

            return string.Join('-', plateNumberParts); // XX-YYYY-XX
        }
    }
}
