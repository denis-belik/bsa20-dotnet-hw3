﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal InitialBalance = 0;
        public const int ParkingCapacity = 10;
        public const int WithdrawTime = 5_000;
        public const int LogTime = 60_000;

        public const decimal FineRatio = 2.5M;

        public static Dictionary<string, decimal> Tariff = new Dictionary<string, decimal>()
        {
            { "PassengerCar", 2    },
            { "Truck",        5    },
            { "Bus",          3.5M },
            { "Motorcycle",   1    },
        };
    }
}
