﻿using System;
using CoolParking.BL.ConsoleApp.Services;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.BL.ConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ConsoleMenuWebApiService menuService = new ConsoleMenuWebApiService();
            try
            {
                Console.WriteLine("Welcome to the parking interface! Type 'help' to see commands syntax.");
                
                bool b = true;
                while (b)
                {
                    string input = Console.ReadLine();
                    string[] inputArgs = input.Split(" ");

                    string command = inputArgs[0].ToLower();
                    if (command == "exit")
                        break;

                    if(inputArgs.Length > 1) inputArgs[1] = inputArgs[1].ToUpper();

                    switch (command)
                    {
                        case "balance": // Вивести на екран поточний баланс Паркінгу.
                            await menuService.ShowParkingBalance();
                            break;

                        case "capacity": // Вивести на екран поточний баланс Паркінгу.
                            await menuService.ShowCapacity();
                            break;

                        case "free": // Вивести на екран кількість вільних місць на паркуванні (вільно X з Y).
                            await menuService.ShowFreePlaces(); 
                            break;

                        case "vehicles": // Вивести на екран список Тр. засобів , що знаходяться на Паркінгу.  
                            await menuService.ShowAllVehicles();
                            break;

                        case "add":  // Поставити Транспортний засіб на Паркінг.
                            await menuService.AddVehicle(inputArgs);
                            break;

                        case "transactions": // Вивести на екран усі Транзакції Паркінгу за поточний період (до запису у лог).
                            await menuService.ShowLastTransactions();
                            break;

                        case "log": // Вивести на екран історію Транзакцій(зчитавши дані з файлу Transactions.log).
                            await menuService.ShowLog(); 
                            break;

                        case "remove": // Забрати Транспортний засіб з Паркінгу.
                            await menuService.RemoveVehicle(inputArgs);
                            break;

                        case "topup": // Поповнити баланс конкретного Тр. засобу.
                            await menuService.TopUpVehicle(inputArgs);
                            break;

                        case "vehicle": // Посмотреть информацию про конкретное ТС
                            await menuService.ShowVehicleInfo(inputArgs);
                            break;

                        case "help": // Шпаргалка со всеми командами
                            menuService.ShowHelpInfo();
                            break;

                        default:
                            Console.WriteLine("Unknown command.");
                            break;
                    }
                }
            }
            catch (ArgumentException argEx)
            {
                menuService.ConsoleWrite(argEx.Message, ConsoleColor.Red);
                await Main(args);
            }
            catch (InvalidOperationException ioEx)
            {
                menuService.ConsoleWrite(ioEx.Message, ConsoleColor.Red);
                await Main(args);
            }
            catch(HttpRequestException httpEx)
            {
                menuService.ConsoleWrite(httpEx.Message, ConsoleColor.Red);
                await Main(args);
            }
            catch (Exception ex)
            {
                menuService.ConsoleWrite(ex.Message, ConsoleColor.Red);
                await Main(args);
            }
        }
    }
}
