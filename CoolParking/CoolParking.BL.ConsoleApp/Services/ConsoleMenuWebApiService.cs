﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.ConsoleApp.Services
{
    class ConsoleMenuWebApiService
    {
        private HttpClient httpClient = new HttpClient();
        private readonly string apiUri = "https://localhost:44382/api/";

        // parking
        public async Task ShowParkingBalance()
        {
            var response = await httpClient.GetAsync(apiUri + "parking/balance");
            if (response.IsSuccessStatusCode)
            {
                string message = $"Parking balance is {await response.Content.ReadAsStringAsync()}";
                ConsoleWrite(message, ConsoleColor.Green);
            }                
            else
                ConsoleWrite("Something went wrong. Sorry.", ConsoleColor.Red);
        }
        public async Task ShowCapacity()
        {
            var response = await httpClient.GetAsync(apiUri + "parking/capacity");
            if (response.IsSuccessStatusCode)
            {
                string message = $"Parking capacity is {await response.Content.ReadAsStringAsync()}";
                ConsoleWrite(message, ConsoleColor.Green);
            }
            else
                ConsoleWrite("Something went wrong. Sorry.", ConsoleColor.Red);
        }
        public async Task ShowFreePlaces()
        {
            var response = await httpClient.GetAsync(apiUri + "parking/freePlaces");
            if (response.IsSuccessStatusCode)
            {
                decimal freePlaces = JsonConvert.DeserializeObject<decimal>(await response.Content.ReadAsStringAsync());
                string message = $"Available {freePlaces} places.";

                ConsoleColor color = freePlaces == 0 ? ConsoleColor.Red : ConsoleColor.Green;
                ConsoleWrite(message, color);
            }
            else
                ConsoleWrite("Something went wrong. Sorry.", ConsoleColor.Red);
        }


        // vehicles
        public async Task ShowAllVehicles()
        {
            var response = await httpClient.GetAsync(apiUri + "vehicles");
            if(response.IsSuccessStatusCode)
            {
                var vehicles = JsonConvert.DeserializeObject<ReadOnlyCollection<Vehicle>>(await response.Content.ReadAsStringAsync());

                ConsoleWrite("All vehicles:", ConsoleColor.Green);
                foreach (Vehicle vehicle in vehicles)
                {
                    ConsoleColor color = vehicle.Balance > (int)vehicle.VehicleType ? ConsoleColor.Green : ConsoleColor.Red;
                    ConsoleWrite($"     {vehicle.VehicleType} {vehicle.Id}. Balance: {vehicle.Balance}.", color);
                }
            }
            else
                ConsoleWrite("Something went wrong. Sorry.", ConsoleColor.Red);
        }
        public async Task ShowVehicleInfo(string[] inputArgs)
        {
            if (inputArgs.Length < 2)
                throw new ArgumentException("Too few arguments: vehicle [plate number]");
            if (!Vehicle.IsIdValid(inputArgs[1]))
                throw new ArgumentException("Incorrect Id format.");

            var response = await httpClient.GetAsync(apiUri + $"vehicles/{inputArgs[1]}");
            string responseMessage = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(responseMessage);
                ConsoleColor color = vehicle.Balance > (int)vehicle.VehicleType ? ConsoleColor.Green : ConsoleColor.Red;
                ConsoleWrite($"{vehicle.VehicleType} {vehicle.Id}. Balance: {vehicle.Balance}.", color);
            }
            else
                ConsoleWrite(responseMessage, ConsoleColor.Red);
        }
        public async Task AddVehicle(string[] inputArgs)
        {
            if (inputArgs.Length < 4)
                throw new ArgumentException("Too few arguments: add [plate number] [vehicle type] [initial balance]");
            if (inputArgs[1].ToLower() != "random" & !Vehicle.IsIdValid(inputArgs[1]))
                throw new ArgumentException("Incorrect Id format.");

            string id = inputArgs[1].ToLower() == "random" ? Vehicle.GenerateRandomRegistrationPlateNumber() : inputArgs[1];
            VehicleType vehicleType = (VehicleType)Enum.Parse(typeof(VehicleType), CapitalizeString(inputArgs[2]));
            decimal balance = decimal.TryParse(inputArgs[3], out _) ? Convert.ToDecimal(inputArgs[3])
                                                                    : throw new ArgumentException($"{inputArgs[3]} is not correct value for [balance].");

            Vehicle vehicle = new Vehicle(id, vehicleType, balance);
            string json = JsonConvert.SerializeObject(vehicle);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(apiUri + "vehicles", data);

            string responseMessage = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                vehicle = JsonConvert.DeserializeObject<Vehicle>(responseMessage);
                ConsoleWrite($"{vehicle.VehicleType} {vehicle.Id} with balance {vehicle.Balance} was added to the parking lot.", ConsoleColor.Green);
            }
            else
                ConsoleWrite(responseMessage, ConsoleColor.Red);
        }
        public async Task RemoveVehicle(string[] inputArgs)
        {
            if (inputArgs.Length < 2)
                throw new ArgumentException("Too few arguments: add [plate number] [vehicle type] [initial balance]");
            if (!Vehicle.IsIdValid(inputArgs[1]))
                throw new ArgumentException("Incorrect Id format.");

            var response = await httpClient.DeleteAsync(apiUri + $"vehicles/{inputArgs[1]}");

            if (response.IsSuccessStatusCode)
                ConsoleWrite($"Vehicle {inputArgs[1]} was removed from the parking lot.", ConsoleColor.Green);
            else
                ConsoleWrite(await response.Content.ReadAsStringAsync(), ConsoleColor.Red);
        }


        // transactions
        public async Task ShowLastTransactions()
        {
            var response = await httpClient.GetAsync(apiUri + $"transactions/last");

            if (response.IsSuccessStatusCode)
            {
                string json = await response.Content.ReadAsStringAsync();
                TransactionInfo[] lastTransactions = JsonConvert.DeserializeObject<TransactionInfo[]>(json);

                if(lastTransactions.Length == 0)
                {
                    ConsoleWrite("No transactions were made in last period", ConsoleColor.Green);
                    return;
                }
                    
                foreach (var transaction in lastTransactions)
                {
                    ConsoleWrite(transaction.ToString(), ConsoleColor.Green);
                }                
            }              
            else
                ConsoleWrite("Something went wrong. Sorry.", ConsoleColor.Red);
        }
        public async Task ShowLog()
        {
            var response = await httpClient.GetAsync(apiUri + $"transactions/all");

            if (response.IsSuccessStatusCode)
            {
                string json = await response.Content.ReadAsStringAsync();
                string logData = JsonConvert.DeserializeObject<string>(json);
                
                ConsoleWrite(logData, ConsoleColor.Green);
            }
            else
                ConsoleWrite("Something went wrong. Sorry.", ConsoleColor.Red);
        }
        public async Task TopUpVehicle(string[] inputArgs)
        {
            if (inputArgs.Length < 3)
                throw new ArgumentException("Too few arguments: add [plate number] [top up sum]");
            if (!Vehicle.IsIdValid(inputArgs[1]))
                throw new ArgumentException("Incorrect Id format.");

            string id = inputArgs[1];
            decimal sum = decimal.TryParse(inputArgs[2], out _) ? Convert.ToDecimal(inputArgs[2])
                                                                    : throw new ArgumentException($"{inputArgs[2]} is not correct value for [top up sum].");

            WebAPI.Models.TopUpModel topUpModel = new WebAPI.Models.TopUpModel() { Id = id, Sum = sum };

            string json = JsonConvert.SerializeObject(topUpModel);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await httpClient.PutAsync(apiUri + $"transactions/topUpVehicle", data);

            if (response.IsSuccessStatusCode)
            {
                string vehicleJson = await response.Content.ReadAsStringAsync();
                Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(vehicleJson);
                ConsoleColor color = vehicle.Balance > (int)vehicle.VehicleType ? ConsoleColor.Green : ConsoleColor.Red;
                ConsoleWrite($"{vehicle.VehicleType} {vehicle.Id}. Balance: {vehicle.Balance}.", color);
            }
            else
                ConsoleWrite(await response.Content.ReadAsStringAsync(), ConsoleColor.Red);
        }


        // additional
        public void ShowHelpInfo()
        {
            Console.WriteLine("   Use these commands to work with parking system:");
            Console.WriteLine("balance - shows parking balance."); // Вивести на екран поточний баланс Паркінгу.
            Console.WriteLine("free - shows available places in the parking lot."); // Вивести на екран кількість вільних місць на паркуванні (вільно X з Y).
            Console.WriteLine("transactions - shows transactions of the current period (before logging)."); // Вивести на екран усі Транзакції Паркінгу за поточний період (до запису у лог).            
            Console.WriteLine("log - shows all transactions from log file."); // Вивести на екран історію Транзакцій(зчитавши дані з файлу Transactions.log).
            Console.WriteLine("vehicles - shows all vehicles from the parking lot."); // Вивести на екран список Тр. засобів , що знаходяться на Паркінгу.  
            Console.WriteLine("add [plate number] [vehicle type] [initial balance] - adds a vehicle."); // Поставити Транспортний засіб на Паркінг.
            Console.WriteLine("   Examples: add HV-1234-OL Truck 100 | add hv-1234-ol Bus 100 | add random Bus 100 ");
            Console.WriteLine("   Note: use 'random' word as a [plate number] to generate random plate number.");
            Console.WriteLine("remove [plate number] - removes a vehicle."); // Забрати Транспортний засіб з Паркінгу.
            Console.WriteLine("topup [plate number] [top up sum] - tops vehicle up."); // Поповнити баланс конкретного Тр. засобу.

            Console.WriteLine("vehicle [plate number] - shows information about vehicle. ");
            Console.WriteLine("help - shows these hints.");
            Console.WriteLine("exit - terminates application.");
        }


        // helpers
        public string CapitalizeString(string str) => string.Join("", char.ToUpper(str[0]), str[1..str.Length]);
        public void ConsoleWrite(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = Console.BackgroundColor == ConsoleColor.Black ? ConsoleColor.White : ConsoleColor.Black;
        }
    }
}
