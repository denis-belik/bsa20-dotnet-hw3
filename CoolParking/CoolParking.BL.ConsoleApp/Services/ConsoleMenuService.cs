﻿using System;
using System.Collections.ObjectModel;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.BL.ConsoleApp.Services
{
    
    class ConsoleMenuService
    {
        ParkingService _parkingService;
        public ConsoleMenuService(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void ShowParkingBalance() => Console.WriteLine($"Parking balance is {_parkingService.GetBalance()} units.");
        public void ShowCapacity() => Console.WriteLine($"Parking capacity is {Settings.ParkingCapacity}");
        public void ShowParkingIncome() => Console.WriteLine($"Last parking income: {_parkingService.GetLastIncome()} units.");
        public void ShowFreePlaces() => Console.WriteLine($"{_parkingService.GetFreePlaces()} available places from {Settings.ParkingCapacity}");
        public void ShowLastTransactions() => Console.WriteLine($"{_parkingService.GetLastTransactionsInfo()}");
        public void ShowLog() => Console.WriteLine($"{_parkingService.ReadFromLog()}");
        public void ShowAllVehicles()
        {
            ReadOnlyCollection<Vehicle> vehicles = _parkingService.GetVehicles();
            Console.WriteLine("All vehicles:");
            foreach (Vehicle vehicle in vehicles)
            {
                Console.WriteLine($"     {vehicle.VehicleType} {vehicle.Id}. Balance: {vehicle.Balance}.");
            }
        }
        public void AddVehicle(string[] inputArgs)
        {
            if (inputArgs.Length < 4)
                throw new ArgumentException("Too few arguments: add [plate number] [vehicle type] [initial balance]");

            string id = inputArgs[1].ToLower() == "random" ? Vehicle.GenerateRandomRegistrationPlateNumber() : inputArgs[1];
            VehicleType vehicleType = (VehicleType)Enum.Parse(typeof(VehicleType), CapitalizeString(inputArgs[2]));
            decimal balance = decimal.TryParse(inputArgs[3], out _) ? Convert.ToDecimal(inputArgs[3])
                                                                    : throw new ArgumentException($"{inputArgs[3]} is not correct value for [balance].");
            _parkingService.AddVehicle(new Vehicle(id, vehicleType, balance));

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"{vehicleType} {id} with balance {balance} was added to the parking lot.");
            Console.ForegroundColor = Console.BackgroundColor == ConsoleColor.Black ? ConsoleColor.White : ConsoleColor.Black;
        }
        public void RemoveVehicle(string[] inputArgs)
        {
            if (inputArgs.Length < 2)
                throw new ArgumentException("Too few arguments: remove [plate number]");

            string id = inputArgs[1];

            _parkingService.RemoveVehicle(id);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Vehicle {id} was removed from the parking lot.");
            Console.ForegroundColor = Console.BackgroundColor == ConsoleColor.Black ? ConsoleColor.White : ConsoleColor.Black;
        }
        public void TopUpVehicle(string[] inputArgs)
        {
            if (inputArgs.Length < 3)
                throw new ArgumentException("Too few arguments: topup [plate number] [top up sum]");

            string id = inputArgs[1];
            decimal balance = decimal.TryParse(inputArgs[2], out _) ? Convert.ToDecimal(inputArgs[2])
                                                                    : throw new ArgumentException($"{inputArgs[2]} is not correct value for [top up sum].");
            _parkingService.TopUpVehicle(id, balance);

            Vehicle vehicle = _parkingService.GetVehicle(id);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Vehicle's balance was topped up {balance} units.");
            Console.WriteLine($"Current balance: {vehicle.Balance}");
            Console.ForegroundColor = Console.BackgroundColor == ConsoleColor.Black ? ConsoleColor.White : ConsoleColor.Black;
        }

        public void ShowVehicleInfo(string[] inputArgs)
        {
            if (inputArgs.Length < 2)
                throw new ArgumentException("Too few arguments: vehicle [plate number]");

            string id = inputArgs[1];
            Vehicle vehicle = _parkingService.GetVehicle(id) ?? throw new ArgumentException($"Vehicle {id} is not in the parking lot.");

            Console.WriteLine($"{vehicle.VehicleType} {vehicle.Id}. Balance: {vehicle.Balance}.");
        }
        public void ShowHelpInfo()
        {
            Console.WriteLine("   Use these commands to work with parking system:");
            Console.WriteLine("balance - shows parking balance."); // Вивести на екран поточний баланс Паркінгу.
            Console.WriteLine("income - shows income of the current period (before logging)."); // Вивести на екран суму зароблених коштів за поточний період(до запису у лог).
            Console.WriteLine("free - shows available places in the parking lot."); // Вивести на екран кількість вільних місць на паркуванні (вільно X з Y).
            Console.WriteLine("transactions - shows transactions of the current period (before logging)."); // Вивести на екран усі Транзакції Паркінгу за поточний період (до запису у лог).            
            Console.WriteLine("log - shows all transactions from log file."); // Вивести на екран історію Транзакцій(зчитавши дані з файлу Transactions.log).
            Console.WriteLine("vehicles - shows all vehicles from the parking lot."); // Вивести на екран список Тр. засобів , що знаходяться на Паркінгу.  
            Console.WriteLine("add [plate number] [vehicle type] [initial balance] - adds a vehicle."); // Поставити Транспортний засіб на Паркінг.
            Console.WriteLine("   Examples: add HV-1234-OL Truck 100 | add hv-1234-ol Bus 100 | add random Bus 100 ");
            Console.WriteLine("   Note: use 'random' word as a [plate number] to generate random plate number.");
            Console.WriteLine("remove [plate number] - removes a vehicle."); // Забрати Транспортний засіб з Паркінгу.
            Console.WriteLine("topup [plate number] [top up sum] - tops vehicle up."); // Поповнити баланс конкретного Тр. засобу.

            Console.WriteLine("vehicle [plate number] - shows information about vehicle. ");
            Console.WriteLine("help - shows these hints.");
            Console.WriteLine("exit - terminates application.");
        }
        public string CapitalizeString(string str)
        {
            return string.Join("", char.ToUpper(str[0]), str[1..str.Length]);
        }

        public void ConsoleWrite(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = Console.BackgroundColor == ConsoleColor.Black ? ConsoleColor.White : ConsoleColor.Black;
        }
    }
}
